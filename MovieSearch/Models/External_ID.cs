﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.Models
{
    public class External_ID
    {
        public string id { get; set; }
        public string imdb_id { get; set; }
        public string facebook_id { get; set; }
        public string instagram_id { get; set; }
        public string twitter_id { get; set; }
    }
}