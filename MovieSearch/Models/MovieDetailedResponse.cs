﻿using Movies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieSearch.Models
{
    public class MovieDetailedResponse
    {
        public List<Movie> movie_results { get; set; }
    }
}