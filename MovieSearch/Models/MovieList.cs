﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Movies.Models
{
    public class MovieList
    {
        public int page { get; set; }
        public List<Movie> results { get; set; }
        public int total_pages { get; set; }
        public int total_results { get; set; }
    }
}