﻿using Movies.Models;
using MovieSearch.Models;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace MovieSearch.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        // Search movies based on Keyword
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ConsumeExternalAPI(string searchKeyword)
        {
            string url = "http://api.themoviedb.org/3/search/movie?api_key=254cd0cc0c6a51ca23b6ab15849c78f6&query=" + searchKeyword;

            var responseString = await getUrlresponse(url);
            var model = JsonConvert.DeserializeObject<MovieList>(responseString);
            if (model.total_results > 0)
            {
                ViewBag.Status = true;
                ViewBag.Model = model;
            }
            else
            {
                ViewBag.Status = false;
            }


            return View();
        }

        // Search for particular movies based on Movie ID
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> MovieDetailed(string movieId)
        {
            // Get external ID using the Movie ID
            string external_id_url = "https://api.themoviedb.org/3/movie/" + movieId + "/external_ids?api_key=254cd0cc0c6a51ca23b6ab15849c78f6";

            var responseString = await getUrlresponse(external_id_url);
            var external_id_model = JsonConvert.DeserializeObject<External_ID>(responseString);

            // Get detailed model using the External ID
            string detailed_url = "https://api.themoviedb.org/3/find/" + external_id_model.imdb_id + "?api_key=254cd0cc0c6a51ca23b6ab15849c78f6&language=en-US&external_source=imdb_id";
            responseString = await getUrlresponse(detailed_url);
            var movie_detailed_Model = JsonConvert.DeserializeObject<MovieDetailedResponse>(responseString);
            movie_detailed_Model.movie_results[0].poster_path = "https://image.tmdb.org/t/p/w500" + movie_detailed_Model.movie_results[0].poster_path;

            // return Detailed movie view
            return View(movie_detailed_Model);
        }

        // Asynchronous call the Web API
        public async System.Threading.Tasks.Task<String> getUrlresponse(string url)
        {
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                System.Net.Http.HttpResponseMessage response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return data;
                }
                return "Failed";

            }
        }

    }
}