# MovieSearch

This is a web project which uses an external web API(https://www.themoviedb.org/documentation/api) to get movie details based on the input. You can search for a particular movie or with a keyword and will find a list of movies that match the name. Further clicking on the movie title will take you to the detailed view showing the poster and other details about the movie.

## Steps to setup:
----------------------------
# 1.Run the application
- Clone and load the project in Visual studio and run it.
- You will land into the home page where you will see a search button
- Clicking on this button will land you on search detail page
- There will be a textbook and button for searching. Type in the keyword to searcj for a movie.
- Movies matching the keyword is listed in the table below
- You can click on the title of each movie which will display the detailed view of the movie.

